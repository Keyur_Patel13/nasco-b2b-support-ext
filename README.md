**setup instructions**

This repo contains all the extensions that are required for ENASCO b2b implementation, compatible with hybris 6.2 setup

    clone this repo in a folder xyz under hybris/bin/
    update the localextensions.xml from enasco-b2b-config repo to the path xyz for auto load extensions

**contribution guidelines**

Please make sure to add JIRA ticket number as a part of all commit messages examples

    [ENSCO-10] intial readme setup
    [ENSCO-10, ENSCO-9] git setup